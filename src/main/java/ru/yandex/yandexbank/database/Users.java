package ru.yandex.yandexbank.database;

public class Users {
    public static final String TABLE = "users";
    public static final String ID = "id";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String COUNTRY = "country";
    public static final String GENDER = "gender";
}
