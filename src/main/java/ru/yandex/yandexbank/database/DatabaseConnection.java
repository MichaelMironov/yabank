package ru.yandex.yandexbank.database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseConnection extends Configuration{
    Connection dbConnection;

    public Connection getDbConnection() throws ClassNotFoundException, SQLException {
        String connection = "jdbc:mysql://" + dbHost + ":"
                + dbPort + "/" + dbName;
        Class.forName("com.mysql.jdbc.Driver");

        dbConnection = DriverManager.getConnection(connection, dbUser, dbPass);

        return dbConnection;
    }

    public  void signUp(String login, String password, String firstname, String lastname,
                        String country, String gender) throws ClassNotFoundException, SQLException {
        String insert = "INSERT INTO " + Users.TABLE + "(" + Users.FIRSTNAME + "," +
                Users.LASTNAME + "," + Users.LOGIN + "," + Users.PASSWORD + "," +
                Users.COUNTRY + "," + Users.GENDER + ")" +
                "VALUES(?,?,?,?,?,?)";

        PreparedStatement prSt = getDbConnection().prepareStatement(insert);
        prSt.setString(1, firstname);
        prSt.setString(2, lastname);
        prSt.setString(3, login);
        prSt.setString(4, password);
        prSt.setString(5, country);
        prSt.setString(6, gender);

        prSt.executeUpdate();
    }
}
