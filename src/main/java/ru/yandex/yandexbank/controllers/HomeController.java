package ru.yandex.yandexbank.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

public class HomeController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ImageView imgGoHomeBtn;

    @FXML
    void initialize() {
        assert imgGoHomeBtn != null : "fx:id=\"imgGoHomeBtn\" was not injected: check your FXML file 'app.fxml'.";
    }

}
