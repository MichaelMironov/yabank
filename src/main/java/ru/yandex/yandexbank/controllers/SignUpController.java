package ru.yandex.yandexbank.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import ru.yandex.yandexbank.database.DatabaseConnection;

public class SignUpController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ToggleGroup Gender;

    @FXML
    private Button backFromSign;

    @FXML
    private Button confirmButton;

    @FXML
    private TextField country;

    @FXML
    private RadioButton female;

    @FXML
    private TextField firstname;

    @FXML
    private Label gender;

    @FXML
    private TextField lastname;

    @FXML
    private TextField login;

    @FXML
    private RadioButton male;

    @FXML
    private TextField password;

    @FXML
    void initialize() {

        backFromSign.setOnAction(actionEvent -> {
            backFromSign.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/ru/yandex/yandexbank/hello-view.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Parent parent = loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(parent));
            stage.show();

        });

        DatabaseConnection dbConnection = new DatabaseConnection();

        confirmButton.setOnAction(actionEvent -> {
            try {
                dbConnection.signUp(firstname.getText(), lastname.getText(), login.getText(), password.getText(),
                        country.getText(), "Male");
            } catch (ClassNotFoundException | SQLException e) {
                throw new RuntimeException(e);
            }
        });
    }

}
