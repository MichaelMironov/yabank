module ru.yandex.yandexbank {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.bootstrapfx.core;
    requires java.sql;
    requires mysql.connector.java;

    opens ru.yandex.yandexbank to javafx.fxml;
    exports ru.yandex.yandexbank;
    exports ru.yandex.yandexbank.controllers;
    opens ru.yandex.yandexbank.controllers to javafx.fxml;
    exports ru.yandex.yandexbank.database;
    opens ru.yandex.yandexbank.database to javafx.fxml;
}